package com.sun.monitor.controller;

import com.sun.monitor.config.PropertiesConfig;
import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.exception.TencentCloudSDKException;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.common.profile.HttpProfile;
import com.tencentcloudapi.sms.v20210111.SmsClient;
import com.tencentcloudapi.sms.v20210111.models.SendSmsRequest;
import com.tencentcloudapi.sms.v20210111.models.SendSmsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("refresh")
@RefreshScope
public class MyController {

    @Autowired
    private PropertiesConfig propertiesConfig;


    @GetMapping("test01")
    public String test01() {
        return propertiesConfig.toString();
    }

    @RequestMapping("/sent")
    public String sendSms() {
        Credential cred = new Credential("AKID32g7PYZRAnHafjbLtIWZR9h7NYYP7fGh", "RgmgS4XoyvmT2Vow9RdrunXAdw9EDFMU");
        // 实例化一个http选项，可选的，没有特殊需求可以跳过
        HttpProfile httpProfile = new HttpProfile();
        httpProfile.setEndpoint("sms.tencentcloudapi.com");
        // 实例化一个client选项，可选的，没有特殊需求可以跳过
        ClientProfile clientProfile = new ClientProfile();
        clientProfile.setHttpProfile(httpProfile);
        // 实例化要请求产品的client对象,clientProfile是可选的
        SmsClient client = new SmsClient(cred, "ap-nanjing");
        // 实例化一个请求对象,每个接口都会对应一个request对象
        SendSmsRequest req = new SendSmsRequest();
        String[] phoneNumberSet1 = {""};
        req.setPhoneNumberSet(phoneNumberSet1);

        req.setSmsSdkAppId("1400835948");
        req.setSignName("深蓝DarkBlue");
        req.setTemplateId("1852768");
        String[] templateParamSet1 = {"10001"};
        req.setTemplateParamSet(templateParamSet1);
        // 返回的resp是一个SendSmsResponse的实例，与请求对象对应
        SendSmsResponse resp = null;
        try {
             resp = client.SendSms(req);
        } catch (TencentCloudSDKException e) {
            e.printStackTrace();
          }
        // 输出json格式的字符串回包
        System.out.println(resp.getSendStatusSet()[0].getMessage());
        return resp.getSendStatusSet()[0].getMessage();
    }
}
