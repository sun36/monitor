package com.sun.monitor.entity;

//文件属性
public class FileEntity {
    private String path;
    private long lastModify;
    private long currentModify;

    public FileEntity(String path, long lastModify) {
        this.path = path;
        this.lastModify = lastModify;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public long getLastModify() {
        return lastModify;
    }

    public void setLastModify(long lastModify) {
        this.lastModify = lastModify;
    }

    public long getCurrentModify() {
        return currentModify;
    }

    public void setCurrentModify(long currentModify) {
        this.currentModify = currentModify;
    }
}
