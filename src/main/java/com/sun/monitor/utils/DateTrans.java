package com.sun.monitor.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTrans {
    public static long timeMillisToLong(long input) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        String formattedTime = sdf.format(new Date(input));
        return Long.parseLong(formattedTime);
    }
}
