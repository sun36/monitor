package com.sun.monitor.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@RefreshScope
@Component
public class PropertiesConfig {
    @Value("${secretid}")
    public  String secretId;
    @Value("${secretidretkey}")
    public String secretKey;
    @Value("${phoneNumberSet}")
    public String[] phoneNumberSet;
    @Value("${smsSdkAppId}")
    public String smsSdkAppId;
    @Value("${signName}")
    public String signName;
    @Value("${templateId}")
    public String templateId;
    @Value("${templateParamSet}")
    public String[] templateParamSet;

    public String getSecretId() {
        return secretId;
    }

    public void setSecretId(String secretId) {
        this.secretId = secretId;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String[] getPhoneNumberSet() {
        return phoneNumberSet;
    }

    public void setPhoneNumberSet(String[] phoneNumberSet) {
        this.phoneNumberSet = phoneNumberSet;
    }

    public String getSmsSdkAppId() {
        return smsSdkAppId;
    }

    public void setSmsSdkAppId(String smsSdkAppId) {
        this.smsSdkAppId = smsSdkAppId;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String[] getTemplateParamSet() {
        return templateParamSet;
    }

    public void setTemplateParamSet(String[] templateParamSet) {
        this.templateParamSet = templateParamSet;
    }

    @Override
    public String toString() {
        return "PropertiesConfig{" + "secretId='" + secretId + '\'' + ", secretKey='" + secretKey + '\'' + ", phoneNumberSet=" + Arrays.toString(phoneNumberSet) + ", smsSdkAppId='" + smsSdkAppId + '\'' + ", signName='" + signName + '\'' + ", templateId='" + templateId + '\'' + ", templateParamSet=" + Arrays.toString(templateParamSet) + '}';
    }
}
