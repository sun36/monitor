package com.sun.monitor.task;


import com.sun.monitor.config.PropertiesConfig;
import com.sun.monitor.entity.FileEntity;
import com.sun.monitor.utils.DateTrans;
import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.exception.TencentCloudSDKException;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.common.profile.HttpProfile;
import com.tencentcloudapi.sms.v20210111.SmsClient;
import com.tencentcloudapi.sms.v20210111.models.SendSmsRequest;
import com.tencentcloudapi.sms.v20210111.models.SendSmsResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

@Component
@Configuration
@EnableScheduling
@RefreshScope
public class ScheduleTask {
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private List<FileEntity> fileEntities = new ArrayList<>();

    @Value("${filePath}")
    private  String filePath;

//    @Value("${scheduleTime}")
//    private String scheduleTime;
    @Value("${secretid}")
    private String secretId;
    @Value("${secretkey}")
    private String secretKey;
    @Value("${phoneNumberSet}")
    private String[] phoneNumberSet;
    @Value("${smsSdkAppId}")
    private String smsSdkAppId;
    @Value("${signName}")
    private String signName;
    @Value("${templateId}")
    private String templateId;
    @Value("${templateParamSet}")
    private String[] templateParamSet;

    @Autowired
    private PropertiesConfig propertiesConfig;

    @Scheduled(fixedRate = 10000)
    private void monitorSysFiles() throws IOException {
        System.out.println("==============================================");
        System.out.println(DateTrans.timeMillisToLong(System.currentTimeMillis()));
        System.out.println("Begin check file status");
        fileEntities.clear();
        boolean modifyFlag = false;
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {

            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split(",");
                if (parts.length == 2) {
                    String path = parts[0].trim();
                    long lastModify = Long.valueOf((parts[1].trim()));
                    File file = new File(path);
                    if (!file.exists()) {
                        System.out.println("Warning: file: " + path + "not exist!");
                        continue;
                    }
                    long currentModify = DateTrans.timeMillisToLong(file.lastModified());
                    FileEntity fileEntity = new FileEntity(path, currentModify);
                    // 根据条件修改对象
                    if (lastModify == 0 || lastModify < currentModify) {
                        modifyFlag = true;
                        System.out.println("Warning: file: " + path + " was changed!!!");
                        fileEntity.setLastModify(currentModify);
                    }
                    fileEntities.add(fileEntity);
                } else {
                    System.out.println("Error: filelist format error!!");
                }
            }


        } catch (IOException e) {
            // 处理文件读取异常
        }

        // 更新对象列表
        if (modifyFlag) {
            writePersonsToFile();
            sendSms();
            System.out.println("file status unnormal! send AlarmXXXX");
        } else {
            System.out.println("file status normal!");
        }
        System.out.println("End check file status");

    }

    public void writePersonsToFile() throws IOException {
        renameFile();
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filePath))) {
            for (FileEntity entity : fileEntities) {
                String line = entity.getPath() + "," + entity.getLastModify();
                writer.write(line);
                writer.newLine();
            }
        } catch (IOException e) {
            // 处理文件写入异常
        }
    }

    public void renameFile( ) throws IOException {
        Path sourcePath = Paths.get(filePath);
        String newFilePath = filePath + DateTrans.timeMillisToLong(System.currentTimeMillis());
        Path targetPath = Paths.get(newFilePath);
        Files.copy(sourcePath, targetPath, StandardCopyOption.COPY_ATTRIBUTES);
        FileWriter writer = new FileWriter(filePath, false);
        writer.close();
    }

    private void sendSms() {
        Credential cred = new Credential(secretId, secretKey);
        // 实例化一个http选项，可选的，没有特殊需求可以跳过
        HttpProfile httpProfile = new HttpProfile();
        httpProfile.setEndpoint("sms.tencentcloudapi.com");
        // 实例化一个client选项，可选的，没有特殊需求可以跳过
        ClientProfile clientProfile = new ClientProfile();
        clientProfile.setHttpProfile(httpProfile);
        // 实例化要请求产品的client对象,clientProfile是可选的
        SmsClient client = new SmsClient(cred, "ap-nanjing");
        // 实例化一个请求对象,每个接口都会对应一个request对象
        SendSmsRequest req = new SendSmsRequest();
        req.setPhoneNumberSet(phoneNumberSet);
        req.setSmsSdkAppId(smsSdkAppId);
        req.setSignName(signName);
        req.setTemplateId(templateId);
        req.setTemplateParamSet(templateParamSet);
//        req.setSmsSdkAppId(propertiesConfig.secretId);
//        req.setSignName(propertiesConfig.getSignName());
//        req.setTemplateId(propertiesConfig.getTemplateId());
//        req.setTemplateParamSet(propertiesConfig.getTemplateParamSet());

        System.out.println("Req signName：" + req.getSignName());
//         返回的resp是一个SendSmsResponse的实例，与请求对象对应
        SendSmsResponse resp = null;
        try {
            resp = client.SendSms(req);
        } catch (TencentCloudSDKException e) {
            e.printStackTrace();
        }

        // 输出json格式的字符串回包
        System.out.println("msg result: " + resp.getSendStatusSet()[0].getMessage());
    }

}